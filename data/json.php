<?php
header('Access-Control-Allow-Origin: *');
    include "koneksi.php";
    $fungsi = $_REQUEST["fungsi"];
    if($fungsi == "data_monitoring"){
        $data = array();
        $sql = "SELECT id, status, SUM(nilai_sensor) AS nilai_sensor, DATE(tgl) tgl FROM tbl_monitoring GROUP BY tgl ORDER BY id DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "rpm"){
        $data = array();
        $sql = "SELECT * FROM tbl_rpm ORDER BY id_rpm DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "hari_ini"){
        $data = array();
        $tgl = date('Y-m-d');
        $sql = "SELECT SUM(nilai_sensor) AS nilai_sensor FROM tbl_monitoring WHERE tgl='$tgl'";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "minggu_ini"){
        $data = array();
        $sql = "SELECT SUM(nilai_sensor) AS nilai_sensor, YEARWEEK(tgl) AS tgl, COUNT(*) AS tgl FROM tbl_monitoring WHERE YEARWEEK(tgl)=YEARWEEK(NOW()) GROUP BY YEARWEEK(tgl)";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "bulan_ini"){
        $data = array();
        $sql = "SELECT SUM(nilai_sensor) AS nilai_sensor, CONCAT(YEAR(tgl),'/',MONTH(tgl)) AS tahun_bulan, COUNT(*) AS jumlah_bulanan FROM tbl_monitoring WHERE CONCAT(YEAR(tgl),'/',MONTH(tgl))=CONCAT(YEAR(NOW()),'/',MONTH(NOW())) GROUP BY YEAR(tgl),MONTH(tgl)";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }

    // ------------------------------ UAS-----------------------------------------------
	if($fungsi == "data_monitoring_ruang_a"){
        $data = array();
        $sql = "SELECT id, status, SUM(nilai_sensor) AS nilai_sensor, DATE(tgl) tgl FROM tbl_monitoring GROUP BY tgl ORDER BY id DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "hari_ini_uas"){
        $data = array();
        $tgl = date('Y-m-d');
        $sql = "SELECT SUM(nilai_sensor) AS result FROM tbl_monitoring WHERE tgl='$tgl' ";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "minggu_ini_uas"){
        $data = array();
        $sql = "SELECT SUM(nilai_sensor) AS nilai_sensor, YEARWEEK(tgl) AS tgl, COUNT(*) AS tgl FROM tbl_monitoring WHERE YEARWEEK(tgl)=YEARWEEK(NOW()) GROUP BY YEARWEEK(tgl)";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "bulan_ini_uas"){
        $data = array();
        $sql = "SELECT SUM(nilai_sensor) AS result FROM tbl_monitoring WHERE CONCAT(YEAR(tgl),'/',MONTH(tgl))=CONCAT(YEAR(NOW()),'/',MONTH(NOW())) GROUP BY YEAR(tgl),MONTH(tgl)";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }

    if($fungsi == "rpm_ruang_a"){
        $data = array();
        $sql = "SELECT * FROM tbl_rpm ORDER BY id_rpm DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
?>
