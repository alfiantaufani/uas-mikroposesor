hari_ini();
minggu_ini();
bulan_ini();

function hari_ini() {
    $.getJSON('https://127.0.0.1/uas_mikroprosesor/data/json.php', {fungsi: "hari_ini_uas"}, function(result){
        var hari_ini = "";
        $.each(result, function(i, k){
            var nilai_sensor = k.result;// total
            var liter = " Liter";
            if (nilai_sensor > 0) {
                hari_ini += nilai_sensor + liter;
            }else{
                hari_ini += '0' + liter;
            }
        });
        $("#hari_ini").html(hari_ini);
    });
}setInterval(hari_ini, 5000);

function minggu_ini() {
    $.getJSON('https://127.0.0.1/uas_mikroprosesor/data/json.php', {fungsi: "minggu_ini_uas"}, function(result){
        var minggu_ini = "";
        $.each(result, function(i, k){
            var nilai_sensor = k.nilai_sensor   ;// total
            var liter = " Liter";
            if (nilai_sensor > 0) {
                minggu_ini += nilai_sensor + liter;
            }else{
                minggu_ini += '0' + liter;
            }
        });
        $("#minggu_ini").html(minggu_ini);
    });
}setInterval(minggu_ini, 5000);

function bulan_ini() {
    $.getJSON('https://127.0.0.1/uas_mikroprosesor/data/json.php', {fungsi: "bulan_ini_uas"}, function(result){
        var bulan_ini = "";
        $.each(result, function(i, k){
            var nilai_sensor = k.result;// total
            var liter = " Liter";
            if (nilai_sensor > 0) {
                bulan_ini += nilai_sensor + liter;
            }else{
                bulan_ini += '0' + liter;
            }
        });
        $("#bulan_ini").html(bulan_ini);
    });
}setInterval(bulan_ini, 5000);

// --------------------- RUANG A ------------------------------------------
ml_ruang_a()
history_ruang_a();       

function ml_ruang_a() {
    $.getJSON('https://127.0.0.1/uas_mikroprosesor/data/json.php', {fungsi: "rpm_ruang_a"}, function(result){
        var dataa = [];
        var ml = "";
        $.each(result, function(i, k){
            var id = k.id_rpm;
            var mili_liter = k.mili_liter;// total

            ml += mili_liter + ' <small>mL</small>';                    
        });
        $("#ml_ruang_a").html(ml);
    });
}setInterval(ml_ruang_a, 500);
      
// menampilkan grafik ruang A
$.getJSON('https://127.0.0.1/uas_mikroprosesor/data/json.php', {fungsi: "data_monitoring_ruang_a"}, function(result){
    var nilai_sensorr = [];
    var tgll = [];
    $.each(result, function(i, kolom){
        var id = kolom.id;
        var nilai_sensor = kolom.nilai_sensor;
        var tgl = kolom.tgl;

        nilai_sensorr.push(nilai_sensor);
        tgll.push(tgl);
    });
    
    grafik_a(nilai_sensorr, tgll);
});

function grafik_a(nilai_sensorr, tgll) {

   var options2 = {
        chart: {
            height: 350,
            type: 'area',
        },
        dataLabels: {
            enabled: false,
        },
        stroke: {
            curve: 'smooth'
        },
        series: [
                  {
                      name: 'Penggunaan Air perLiter',
                      data: nilai_sensorr
                  } 
                ],

        xaxis: {
            type: 'datetime',
            categories: tgll,
            labels: {
              style: {
                colors: 'rgba(94, 96, 110, .5)'
              }
            }           
        },
        tooltip: {
            theme:'dark',
            x: {
                format: 'dd-MM-yyyy'
            },
        },
        grid: {
          borderColor: 'rgba(94, 96, 110, .5)',
          strokeDashArray: 4
        },
        noData: {
            text: 'Mohon Tunggu...'
        }
    }

    var chart2 = new ApexCharts(
        document.querySelector("#apexx"),
        options2
    );

    chart2.render();
}

function history_ruang_a() {
    var hasil = "";
    $.getJSON('https://127.0.0.1/uas_mikroprosesor/data/json.php', {fungsi: "data_monitoring_ruang_a"}, function(result){
		$.each(result, function(i, kolom){
            var id = kolom.id;
            var nilai_sensor = kolom.nilai_sensor;
            var tgl = kolom.tgl;
            var status = kolom.status;
            
            hasil += '\
                <div class="support-ticket media pb-1 mb-3">\
                    <div class="media-body ml-3">\
                        <button data-kode="'+tgl+'" onclick="hapus_a(this)" class="btn btn-pill btn-danger mb-1 float-right"><i class="fas fa-trash-alt"></i></button>\
                        <span class="font-weight-bold">'+ nilai_sensor +' Liter Penggunaan Air</span>\
                        <p class="my-1"> </p>\
                        <small class="text-muted">'+ status +' Pada Tanggal - '+ tgl +'</small>\
                    </div>\
                </div>';
        });
		$("#histori").html(hasil);
	});        
}setInterval(history_ruang_a, 2500);
   

// Hapus History ruang A
function hapus_a(el){
    var x = $(el).data("kode");
    var url = "https://127.0.0.1/uas_mikroprosesor/data/kirimdata.php";
    var t = "Anda yakin ingin menghapus data tanggal " + x + "?";
    swal({
      title: "PERINGATAN",
      text: t,
      icon: "warning",
      buttons: [
        'TIDAK',
        'YA'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        //ajax delete
        $.ajax({
          url:  url,
          data:   { delete:x },
          dataType: 'JSON',
          type: 'GET',
          success: function (response) {
            swal('Sukses', 'You clicked the button!', 'success', {
                buttons: false,
                timer: 1000,
              }).then(function() {
              location.reload();
            });
            
          }
        });

      } else {
        return true;
      }
    })
}